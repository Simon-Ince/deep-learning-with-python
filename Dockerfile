FROM continuumio/anaconda3:4.4.0

COPY . /code
WORKDIR /code
RUN conda create -n tfdeeplearning python=3.5 -y && conda install jupyter numpy pandas scikit-learn matplotlib -y
RUN pip install --upgrade pip && pip install --upgrade tensorflow

EXPOSE 8888
