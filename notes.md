# Supervised Learning

Uses labeled data to predict a label given some features.  
If the label is continuous its called a regression problem, if its categorical it is a classification problem.

Supervised Learning uses labeled data to predict a label given some features.  
If the label is continuous its called a regression problem, if its categorical it is a classification problem.

### Supervised Learning - Classification
We have all features, we're trying to classify.  
Features: Height and Weights  
Label: Gender  
Task: Given a person’s height and weight, predict their gender.

### Supervised Learning - Regression
We have one of more features, we're trying to find a continuous label.  
Features: Square Footage, Rooms  
Label: House Price  
Task: Given a house’s size and number of rooms, predict the selling price.

Supervised Learning has the model train on historical data that is already labeled (e.g. previous house sales).  
Once the model is trained, it can then be used on new data, where only the features are known, to attempt prediction

# Unsupervised Learning

But what if you don’t have historical labels for your data? (You only have features)  
Since you have no “right answer” to fit on, you need to look for patterns in the data and find a structure.

### Unsupervised Learning - Clustering
Features: Heights and Weights for breeds of dogs.  
Label: No Label for unsupervised!  
Task: Cluster together the data into similar groups. It is then up to the data scientist to interpret the clusters.

Clustering won’t be able to tell you what the group labels should be. Only that the points in each cluster are similar to each other based off the features.

# Reinforcement Learning

What about machine learning tasks like have a computer learn to play a video game, drive a car, etc… ?  
Reinforcement learning works through trial and error which actions yield the greatest rewards.

Components  
Agent-Learning/Decision  Maker  
Environment - What Agent interacts with  
Actions - What the Agent can do

The agent chooses actions that maximize some specified reward metric over a given amount of time.  
Learning the best policy with the environment and responding with the best actions.

# basic machine learning process

![basic machine learning process](imgs/1.png)

Unsupervised Learning will not have a training set as we do not know the correct labels.

A "Holdout" set is used after the testing cycle to check we are not just adjusting for our training data.

# Evaluation

### Supervised Learning -Classification Eval
Accuracy , Recall, Precision  
Accuracy - Correctly Classified divided by total samples.  
Which metric is the most important depends on the specific situation  
MAE, MSE, RMSE  
All are measurements of: On average, how far off are you from the correct continuous value.

### Unsupervised Learning - Evaluation
Much harder to evaluate, depends on overall goal of the task  
Never had “Correct Labels” to compare to  
Cluster Homogeneity, Rand Index

![Unsupervised Learning - Evaluation](imgs/2.png)

### Reinforcement Learning - Evaluation
Usually more obvious, since the “evaluation” is built into the actual training of the model.  
How well the model performs the task its assigned.
