# Tensorflow-Bootcamp in Docker

build the Docker image:
`docker-compose build`

Run and jump into the container:
`docker-compose run -p 8888:8888 anaconda3`

Activate:
`source activate tfdeeplearning`

Run jupyter notebook:
`jupyter notebook --ip='*' --port=8888 --no-browser --allow-root`

You'll be shown a URL to vist in your browser to access jupyter.
